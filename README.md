# Seed
专注于快速Web开发的全栈式框架

Seed-MVC使用方法详见
[stbackground](http://git.oschina.net/opdar/stbackground)
支持restful，类似于springmvc的操作，支持自定义content-type流识别

Seed-Database
简易的数据库操作框架，可实现ORM，已实现增删改查支持事务。

Seed-Template为模板引擎，自实现语法解析，目前正在不断开发完善中，已可以使用以下功能：

变量定义，如：
var a = 100; //100
var b = "seed"+a; //seed100
var c = 1+3*(2+4); //19

打印语句，如：
printf(Hello world!${a})

for循环，如：
for(object in objects){...}

switch-case，如：
switch(..){case 1:break;case 2:break;}

CPlan项目可使Seed-MVC开发应用无缝集成到现有项目中，以便达到逐步替换的目的。

使用CPlan模块集成的项目必须在生成的模块包下实现package.json，如：

```
{
  "module-name":"users",
  "desc":"用户管理",
  "main":"com.xxx.background.module.plan.base.UsersEntry",
  "controllers":"com.xxx.background.module.plan.controller"
}
```
main为入口点，模块包被加载时将会调用该类的entry方法，类必须集成com.opdar.cplan.base.Entry
controllers为需要扫描controller的目录，输入路径后将在初始化时自动扫描对应包下的controller

配置方法：
在web.xml中加上以下配置
```
	<listener>
		<listener-class>com.opdar.cplan.plugins.CPServletSupport</listener-class>
	</listener>
	<servlet>
		<servlet-name>ModuleServlet</servlet-name>
		<servlet-class>com.opdar.framework.server.supports.servlet.SeedServlet</servlet-class>
		<load-on-startup>1</load-on-startup>
	</servlet>
	<servlet-mapping>
		<servlet-name>ModuleServlet</servlet-name>
		<url-pattern>/module/*</url-pattern>
	</servlet-mapping>
```
启动后路径的名称由module-name决定，如上方所示的users模块，访问地址为http://.../module/users/...

关于工具类：
使用SeedInvoke可实现快速的类反射操作。
使用YesonParser可实现简单的JSON转换操作。
还有一些奇奇怪怪的功能等待发掘。

关于扩展：
Seed的代码结构极易扩展，现仍在不断完善中，期待更多的人加入。

关于文档：
文档不完善，因为目前为止一直是自用，也没那么多时间完善文档，有能力的话自行阅读代码，也可加入QQ群 372824396 联系一个叫 群主 的人获得帮助。